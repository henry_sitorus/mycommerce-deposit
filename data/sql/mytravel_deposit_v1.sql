-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 17, 2016 at 06:22 AM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytravel_deposit`
--

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE `references` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime_request` datetime NOT NULL,
  `invoice` varchar(20) NOT NULL,
  `reference_number` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `user_id`, `datetime_request`, `invoice`, `reference_number`, `created_at`) VALUES
(5, 1, '2016-04-17 02:59:42', 'AVASD23', 'Adbqv0Jro8lhD8erxoHm', '2016-04-17 05:35:26'),
(6, 1, '2016-04-17 02:59:42', 'AVASD23', 'WyQzC8iQmAuWOFnjG6K4', '2016-04-17 05:35:38'),
(7, 1, '2016-04-17 02:59:42', 'AVASD23', 'f6lTezkMY18UzZuc7M2u', '2016-04-17 05:35:38'),
(8, 1, '2016-04-17 02:59:42', 'AVASD23', 'nxrbcOuTVfXalj3ATomS', '2016-04-17 05:35:39'),
(9, 1, '2016-04-17 02:59:42', 'AVASD23', 'puMZuhbC4e6rLxCXl7Rh', '2016-04-17 05:35:39'),
(10, 1, '2016-04-17 02:59:42', 'AVASD23', 'nPsI8wi2UEUj9HjDYuf2', '2016-04-17 05:35:39'),
(11, 1, '2016-04-17 02:59:42', 'AVASD23', 'IlutT7rffiwC7ZkgvDjp', '2016-04-17 05:35:39'),
(12, 1, '2016-04-17 02:59:42', 'AVASD23', 'idJrU25TxkWfGqJAybPN', '2016-04-17 05:35:40'),
(13, 1, '2016-04-17 02:59:42', 'AVASD23', 'ulpClJSQnbgFpZ7j2cdz', '2016-04-17 05:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invoice` varchar(10) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `code` varchar(5) NOT NULL,
  `desc` varchar(40) NOT NULL,
  `reference_number` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `invoice`, `amount`, `code`, `desc`, `reference_number`, `created_at`, `updated_at`) VALUES
(1, 40, '17042016ow', 123123.00, 'CRD01', 'dfffffffffffffffffffffffffff', '', '2016-04-17 02:59:42', '0000-00-00 00:00:00'),
(2, 40, '17042016S3', 999999.99, 'CRD01', 'deposit awal sejumlaj 1 juta', 'RHrqfOa9Se', '2016-04-17 03:02:06', '0000-00-00 00:00:00'),
(3, 39, '170420167s', 999999.99, 'CRD01', 'asdasd asd asd', 'plUAvMPCxc', '2016-04-17 03:07:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `balance` decimal(8,2) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `desc` varchar(50) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `balance`, `pin`, `desc`, `status`, `created_at`, `updated_at`) VALUES
(38, 'sdkfsdlfk', 0.00, 'lksdlfksl', 'klsdkflsdkl', 'active', '2016-04-15 19:47:11', '0000-00-00 00:00:00'),
(39, 'qeqwoei', 9090.00, 'ad90as9d09', 'asjdkajsdk', 'active', '2016-04-15 19:48:43', '0000-00-00 00:00:00'),
(40, 'henry', 0.00, 'jasdajsdk', 'jasdkjasd', 'active', '2016-04-15 23:09:05', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `references`
--
ALTER TABLE `references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `references`
--
ALTER TABLE `references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;