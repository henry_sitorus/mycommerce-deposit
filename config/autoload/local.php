<?php
return array(
    'statuslib' => array(
        'array_mapper_path' => 'data/statuslib.php',
    ),
    'db' => array(
        'adapters' => array(
            'db' => array(
                'database' => 'my_travel',
                'driver' => 'PDO_Mysql',
                'hostname' => 'localhost',
                'username' => 'root',
                'password' => 'root',
                'dsn' => 'mysql:dbname=mytravel_deposit;host=localhost',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'adapters' => array(
                'oauth2_pdo' => array(
                    'adapter' => 'ZF\\MvcAuth\\Authentication\\OAuth2Adapter',
                    'storage' => array(
                        'adapter' => 'pdo',
                        'dsn' => 'mysql:dbname=mytravel_deposit;host=localhost',
                        'route' => '/oauth2',
                        'username' => 'root',
                        'password' => 'root',
                    ),
                ),
            ),
        ),
    ),
);
