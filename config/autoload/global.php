<?php
return array(
    'db' => array(
        'adapters' => array(
            'db' => array(
                'database' => 'apigility',
                'driver' => 'PDO_Mysql',
                'hostname' => 'localhost',
                'username' => 'root',
                'password' => 'root',
                'dsn' => 'mysql:dbname=apigility;host=localhost',
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'oauth' => array(
                'options' => array(
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/oauth2))',
                ),
                'type' => 'regex',
            ),
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'map' => array(
                'Status\\V1' => 'oauthadapter',
                'Organizations\\V1' => 'oauth2_pdo',
            ),
        ),
    ),
    'doctrine' => array(
        'connection' => array(
            'odm_default' => array(
                'server' => 'localhost',
                'port' => '27017',
                'connectionString' => null,
                'user' => null,
                'password' => null,
                'dbname' => 'comedy',
                'options' => array(),
            ),
        ),
        'configuration' => array(
            'odm_default' => array(
                'metadata_cache' => 'array',
                'driver' => 'odm_default',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineMongoODMModule/Proxy',
                'proxy_namespace' => 'DoctrineMongoODMModule\\Proxy',
                'generate_hydrators' => true,
                'hydrator_dir' => 'data/DoctrineMongoODMModule/Hydrator',
                'hydrator_namespace' => 'DoctrineMongoODMModule\\Hydrator',
                'default_db' => 'comedy',
                'filters' => array(),
                'logger' => null,
            ),
        ),
        'driver' => array(
            'ODM_Driver' => array(
                'class' => 'Doctrine\\ODM\\MongoDB\\Mapping\\Driver\\AnnotationDriver',
            ),
            'odm_default' => array(
                'drivers' => array(
                    'flight\\V1\\Document\\Member' => 'ODM_Driver',
                    'flight\\V1\\Document\\Requestorder' => 'ODM_Driver',
                    'flight\\V1\\Document\\Responseorder' => 'ODM_Driver',
                    'flight\\V1\\Document\\Requestfare' => 'ODM_Driver',
                ),
            ),
        ),
        'documentmanager' => array(
            'odm_default' => array(
                'connection' => 'odm_default',
                'configuration' => 'odm_default',
                'eventmanager' => 'odm_default',
            ),
        ),
        'eventmanager' => array(
            'odm_default' => array(
                'subscribers' => array(),
            ),
        ),
    ),
);
