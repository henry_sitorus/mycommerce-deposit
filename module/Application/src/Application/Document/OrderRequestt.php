<?php
namespace flight\V1\Document;
use flight\V1\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

class OrderRequestt {
	private $origin;
	private $destination;
	private $departure_date;
	private $return_date;
	private $trip_type;
	private $adults;
	private $childs;
	private $infants;
	private $airlines;
	public function setOrigin($origin) {
		$this->origin = $origin;
	}
	public function setDestination($destination) {
		$this->destination = $destination;
	}
	public function setDepartureDate($departure_date) {
		$this->departure_date = $departure_date;
	}
	public function setReturnDate($return_date) {
		$this->return_date = return_date;
	}
	public function setTripType($trip_type) {
		$this->trip_type = $trip_type;
	}
	public function setAdults($adults) {
		$this->adults = $adults;
	}
	public function setChilds($childs) {
		$this->childs = $childs;
	}
	public function setInfants($infants) {
		$this->infants = $infants;
	}
	public function setAirlines($airlines) {
		$this->airlines = $airlines;
	}
	public function getOrigin() {
		return $this->origin;
	}
	public function getDestination() {
		return $this->destination;
	}
	public function getDepartureDate() {
		return $this->departure_date;
	}
	public function getReturnDate() {
		return $this->return_date;
	}
	public function getTripType() {
		return $this->trip_type;
	}
	public function getAdults() {
		return $this->adults;
	}
	public function getChilds() {
		return $this->childs;
	}
	public function getInfants() {
		return $this->infants;
	}
	public function getAirlines() {
		return $this->airlines;
	}
}