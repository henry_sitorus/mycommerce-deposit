<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\Balances\Factory;

use DomainException;
use Deposit\V1\Rest\Balances\Mapper;

class BalancesTableGatewayMapperFactory {
	
	
	public function __invoke($services) {
		
		 
		if (! $services->has ( 'Balances\TableGateway' )) {
			throw new DomainException ( 'Cannot create Balances\BalancesTableGatewayMapper; missing Balances\TableGateway dependency' );
		}
		
		
		return new Mapper\BalancesTableGatewayMapper ( $services->get ( 'Balances\TableGateway' ), $services->get ( 'Balances\TableGateway' ) );
	}
}
