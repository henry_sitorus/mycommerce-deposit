<?php

namespace Deposit\V1\Rest\Balances\Factory;

use Deposit\V1\Rest\Balances\Service;

class BalancesResourceFactory {
	public function __invoke($services) {
		return new Service\BalancesResource ( $services->get ( 'Balances\TableGatewayMapper' ) );
	}
}
