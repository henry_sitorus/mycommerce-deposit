<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\Users\Mapper;

use ZF\ApiProblem\ApiProblem;
use DomainException;
use InvalidArgumentException;
use Traversable;
//use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Stdlib\ArrayUtils;
use Zend\Db\Sql\Select;
//use Zend\Db\Sql\Sql, Zend\Db\Sql\Where;

use Deposit\V1\Rest\Users\Mapper;
use Deposit\V1\Rest\Users\Hydrator;
use Assetic\Exception\Exception;
// use Users\V1\Rest\Users\Model;

/**
 * Mapper implementation using a Zend\Db\TableGateway
 */
class UsersTableGatewayMapper implements Mapper\UsersMapperInterface {
	/**
	 *
	 * @var TableGateway
	 */
	protected $table;
	protected $UsersTable;
	protected $response;
	protected $msg;
	protected $code;
	protected $rs;
	
	/**
	 *
	 * @param TableGateway $table        	
	 */
	public function __construct(Hydrator\UsersTableGateway $table,Hydrator\UsersTableGateway $UsersTable) {
		$this->table = $table;
		$this->UsersTable = $UsersTable;
	}
	
	/**
	 *
	 * @param array|Traversable|\stdClass $data        	
	 * @return Entity
	 */
	public function create($data) {
		try {
			
			if ($data instanceof Traversable) {
				$data = ArrayUtils::iteratorToArray ( $data );
			}
			
			if (is_object ( $data )) {
				$data = ( array ) $data;
			}
			
			if (! is_array ( $data )) {
				// throw new InvalidArgumentException ( sprintf ( 'Invalid data provided to %s; must be an array or Traversable', __METHOD__ ) );
				throw new \Exception ( 'wrong data format', '500' );
			}
			
			$data ['created_at'] = date ( 'Y-m-d H:i:s' );
			$this->table->insert ( $data );
			
			$id = $this->table->getLastInsertValue ();
			$resultSet = $this->table->select ( array (
					'id' => $id 
			) );
			
			if (0 === count ( $resultSet )) {
				// throw new DomainException ( 'Insert operation failed or did not result in new row', 500 );
				throw new \Exception ( 'Insert operation failed or did not result in new row', 500 );
			} else {
				$response ['response'] ['params'] = $data;
				$response ['response'] ['message'] = 'Success insert data deposit user';
				$response ['response'] ['code'] = '';
				return $response;
			}
		} catch ( \Exception $e ) {
			// throw new DomainException ( 'Insert operation failed or did not result in new row', 500 );
			// throw Exception ( 'Insert operation failed or did not result in new row', 500 );
			$response ['response'] ['params'] = $data;
			$response ['response'] ['message'] = $e->getMessage ();
			$response ['response'] ['code'] = $e->getCode ();
			return $response;
		}
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return Entity
	 */
	public function fetch($id) {
		try {
			
			if (! $id) {
				throw new \Exception ( 'wrong id ', 500 );
			}
			$sqlSelect = $this->table->getSql ()->select ();
			$sqlSelect->columns ( array (
					'*'
			) );
			$where = new Where ();
			$where->expression ( "organizations.id = ?", $id );
			$sqlSelect->where ( $where );
			
			$statement = $this->table->getSql ()->prepareStatementForSqlObject ( $sqlSelect );
			$rowSet = $statement->execute ();
			if (0 === count ( $rowSet )) {
				throw new \Exception ( 'no data found for id :'.$id, 500 );
			}
			$response ['response'] ['params'] = $rowSet->current ();
			$response ['response'] ['message'] ='';
			$response ['response'] ['code'] = '';
			return $response;
		}catch(\Exception $e){
			$response ['response'] ['params'] = '';
			$response ['response'] ['message'] = $e->getMessage ();
			$response ['response'] ['code'] = $e->getCode ();
			return $response;
		}
	}
	
	/**
	 *
	 * @return Collection
	 */
	public function fetchAll() {
		$resultSet = $this->table->select ( function (Select $select) {
			$select->columns ( array (
					'*' 
			) );
		} );
		
		return $resultSet;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @param array|Traversable|\stdClass $data        	
	 * @return Entity
	 */
	public function update($id, $data) {
		if (! $id) {
			throw new DomainException ( 'Invalid identifier provided', 404 );
		}
		if (is_object ( $data )) {
			$data = ( array ) $data;
		}
		
		$data ['updated_at'] = date ( 'Y-m-d H:i:s' );
		$this->table->update ( $data, array (
				'id' => $id 
		) );
		
		$rowSetUsersAddress = $this->UsersTable->select ( array (
				'id' => $id 
		) );
		
		$Users = $rowSetUsersAddress->current ();
		
		$rowSet = $this->table->select ( array (
				'id' => $id 
		) );
		
		if (0 === count ( $rowSet )) {
			return new ApiProblem ( 404, 'Data not found' );
		}
		
		return $Users;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return bool
	 */
	public function delete($id) {
		if (! $id) {
			throw new DomainException ( 'Invalid identifier provided', 404 );
		}
	  return $this->table->delete(array('id' => $id));
    }
}
