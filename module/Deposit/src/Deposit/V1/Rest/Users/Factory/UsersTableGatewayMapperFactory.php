<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\Users\Factory;

use DomainException;
use Deposit\V1\Rest\Users\Mapper;

class UsersTableGatewayMapperFactory {
	
	
	public function __invoke($services) {
		
		 
		if (! $services->has ( 'Users\TableGateway' )) {
			throw new DomainException ( 'Cannot create Users\UsersTableGatewayMapper; missing Users\TableGateway dependency' );
		}
		
		
		return new Mapper\UsersTableGatewayMapper ( $services->get ( 'Users\TableGateway' ), $services->get ( 'Users\TableGateway' ) );
	}
}
