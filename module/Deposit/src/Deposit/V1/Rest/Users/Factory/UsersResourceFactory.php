<?php

namespace Deposit\V1\Rest\Users\Factory;

use Deposit\V1\Rest\Users\Service;

class UsersResourceFactory {
	public function __invoke($services) {
		return new Service\UsersResource ( $services->get ( 'Users\TableGatewayMapper' ) );
	}
}
