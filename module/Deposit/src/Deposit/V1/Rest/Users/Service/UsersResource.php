<?php

namespace Deposit\V1\Rest\Users\Service;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Deposit\V1\Rest\Users\Mapper;

class UsersResource extends AbstractResourceListener {
	protected $mapper;
	public function __construct(Mapper\UsersMapperInterface $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Create a resource
	 *
	 * @param mixed $data        	
	 * @return ApiProblem|mixed
	 */
	public function create($data) {
		return $this->mapper->create ( $data );
		// return new ApiProblem(405, 'The POST method has not been defined');
	}
	
	/**
	 * Delete a resource
	 *
	 * @param mixed $id        	
	 * @return ApiProblem|mixed
	 */
	public function delete($id) {
		return $this->mapper->delete ( $id );
		// return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
	}
	
	/**
	 * Delete a collection, or members of a collection
	 *
	 * @param mixed $data        	
	 * @return ApiProblem|mixed
	 */
	public function deleteList($data) {
		return new ApiProblem ( 405, 'The DELETE method has not been defined for collections' );
	}
	
	/**
	 * Fetch a resource
	 *
	 * @param mixed $id        	
	 * @return ApiProblem|mixed
	 */
	public function fetch($id) {
		die ( 'asd' );
		
		return $this->mapper->fetch ( $id );
		// if(empty($result)) {
		// return new ApiProblem(404, 'data not found');
		// }
		return $result;
	}
	
	/**
	 * Fetch all or a subset of resources
	 *
	 * @param array $params        	
	 * @return ApiProblem|mixed
	 */
	public function fetchAll($params = array()) {
		return $this->mapper->fetchAll ();
	}
	
	/**
	 * Patch (partial in-place update) a resource
	 * 
	 * @param mixed $id        	
	 * @param mixed $data        	
	 * @return ApiProblem|mixed
	 */
	public function patch($id, $data) {
		return $this->mapper->update ( $id, $data );
		// return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
	}
	
	/**
	 * Replace a collection or members of a collection
	 * 
	 * @param mixed $data        	
	 * @return ApiProblem|mixed
	 */
	public function replaceList($data) {
		return new ApiProblem ( 405, 'The PUT method has not been defined for collections' );
	}
	
	/**
	 * Update a resource
	 * 
	 * @param mixed $id        	
	 * @param mixed $data        	
	 * @return ApiProblem|mixed
	 */
	public function update($id, $data) {
		return $this->mapper->update ( $id, $data );
//        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}
