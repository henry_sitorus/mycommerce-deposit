<?php

namespace Deposit\V1\Rest\Users\Model;

class UsersEntity {
	public $id;
	public $username;
	public $balance;
	public $pin;
	public $status;
	public $desc;
	public $created_at;
	public $updated_at;
}
