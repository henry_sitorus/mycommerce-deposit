<?php

namespace Deposit\V1\Rest\References\Model;

class ReferencesEntity {
	public $id;
	public $user_id;
	public $datetime_request;
	public $invoice;
	public $reference_number;
	public $created_at;
}
