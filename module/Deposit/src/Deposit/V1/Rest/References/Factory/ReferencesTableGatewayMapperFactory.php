<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\References\Factory;

use DomainException;
use Deposit\V1\Rest\References\Mapper;

class ReferencesTableGatewayMapperFactory {
	
	
	public function __invoke($services) {
		
		 
		if (! $services->has ( 'References\TableGateway' )) {
			throw new DomainException ( 'Cannot create References\ReferencesTableGatewayMapper; missing References\TableGateway dependency' );
		}
		
		
		return new Mapper\ReferencesTableGatewayMapper ( $services->get ( 'References\TableGateway' ), $services->get ( 'References\TableGateway' ) );
	}
}
