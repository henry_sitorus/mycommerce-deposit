<?php

namespace Deposit\V1\Rest\References\Factory;

use Deposit\V1\Rest\References\Service;

class ReferencesResourceFactory {
	public function __invoke($services) {
		return new Service\ReferencesResource ( $services->get ( 'References\TableGatewayMapper' ) );
	}
}
