<?php

namespace Deposit\V1\Rest\Transactions\Model;

class TransactionsEntity {
	public $id;
	public $user_id;
	public $invoice;
	public $amount;
	public $code;
	public $desc;
	public $reference_number;
	public $created_at;
	public $updated_at;
}
