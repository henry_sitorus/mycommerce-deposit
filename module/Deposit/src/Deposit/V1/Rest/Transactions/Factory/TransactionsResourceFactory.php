<?php

namespace Deposit\V1\Rest\Transactions\Factory;

use Deposit\V1\Rest\Transactions\Service;

class TransactionsResourceFactory {
	public function __invoke($services) {
		return new Service\TransactionsResource ( $services->get ( 'Transactions\TableGatewayMapper' ) );
	}
}
