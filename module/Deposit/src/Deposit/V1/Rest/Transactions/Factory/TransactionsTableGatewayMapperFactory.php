<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\Transactions\Factory;

use DomainException;
use Deposit\V1\Rest\Transactions\Mapper;

class TransactionsTableGatewayMapperFactory {
	
	
	public function __invoke($services) {
		
		 
		if (! $services->has ( 'Transactions\TableGateway' )) {
			throw new DomainException ( 'Cannot create Transactions\TransactionsTableGatewayMapper; missing Transactions\TableGateway dependency' );
		} 
		return new Mapper\TransactionsTableGatewayMapper ( 
				$services->get ( 'Transactions\TableGateway' ), 
				$services->get ( 'Transactions\TableGateway' ) ,
				$services->get ( 'Transactions\UsersTableGateway' ),
				$services->get ( 'Transactions\ReferencesTableGateway' )
				
				);
	}
}
