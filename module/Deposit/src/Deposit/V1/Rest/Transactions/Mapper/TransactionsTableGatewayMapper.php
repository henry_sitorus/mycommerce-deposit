<?php

/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */
namespace Deposit\V1\Rest\Transactions\Mapper;

use ZF\ApiProblem\ApiProblem;
use DomainException;
// use InvalidArgumentException;
use Traversable;
// use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Stdlib\ArrayUtils;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Deposit\V1\Rest\Transactions\Mapper;
use Deposit\V1\Rest\Transactions\Hydrator;
use Assetic\Exception\Exception;
// use Transactions\V1\Rest\Transactions\Model;

/**
 * Mapper implementation using a Zend\Db\TableGateway
 */
class TransactionsTableGatewayMapper implements Mapper\TransactionsMapperInterface {
	/**
	 *
	 * @var TableGateway
	 */
	protected $table;
	protected $TransactionsTable;
	protected $ReferencesTable;
	protected $UsersTable;
	protected $response;
	protected $msg;
	protected $code;
	protected $rs;
	
	/**
	 *
	 * @param TableGateway $table        	
	 */
	public function __construct(Hydrator\TransactionsTableGateway $table, Hydrator\TransactionsTableGateway $TransactionsTable, Hydrator\TransactionsTableGateway $UsersTable, Hydrator\TransactionsTableGateway $ReferencessTable) 

	{
		$this->table = $table;
		$this->TransactionsTable = $TransactionsTable;
		$this->UsersTable = $UsersTable;
		$this->ReferencesTable = $ReferencessTable;
	}
	
	/**
	 *
	 * @param array|Traversable|\stdClass $data        	
	 * @return Entity
	 */
	function updateBalanceIncrease($userId, $amount) {
		$amountBalance = $this->getBalance ( $userId );
		$balance = $amountBalance ['balance'];
		$data ['balance'] = $balance + $amount;
		$data ['updated_at'] = date ( 'Y-m-d H:i:s' );
		
		return $this->UsersTable->update ( $data, array (
				'id' => $userId 
		) );
	}
	function updateBalanceDecrease($userId, $amount) {
		$amountBalance = $this->getBalance ( $userId );
		$balance = $amountBalance ['balance'];
		$data ['balance'] = $balance - $amount;
		$data ['updated_at'] = date ( 'Y-m-d H:i:s' );
		return $this->UsersTable->update ( $data, array (
				'id' => $userId 
		) );
	}
	function getBalance($userId) {
		$sqlSelect = $this->UsersTable->getSql ()->select ();
		$sqlSelect->columns ( array (
				'*' 
		) );
		$where = new Where ();
		$where->expression ( "id = ?", $userId );
		$sqlSelect->where ( $where );
		$statement = $this->UsersTable->getSql ()->prepareStatementForSqlObject ( $sqlSelect );
		$rowSet = $statement->execute ();
		$result = $rowSet->current ();
		if (! empty ( $result )) {
			return $result;
		} else {
			return array ();
		}
	}
	public function create($data) {
		try {
			
			if ($data instanceof Traversable) {
				$data = ArrayUtils::iteratorToArray ( $data );
			}
			
			if (is_object ( $data )) {
				$data = ( array ) $data;
			}
			
			if (! is_array ( $data )) {
				throw new \Exception ( 'wrong data format', '500' );
			}
			
			if (! $this->checkReferenceNumber ( $data ['reference_number'], $data ['user_id'] )) {
				throw new \Exception ( 'reference number not found', '500' );
			}
			
			$userid = $data ['user_id'];
			$amount = $data ['amount'];
			
			if ($data ['code'] == 'CRD01') {
				$this->updateBalanceIncrease ( $userid, $amount );
			} elseif ($data ['code'] == 'DBT01') {
				$this->updateBalanceDecrease ( $userid, $amount );
			}
			
			$data ['created_at'] = date ( 'Y-m-d H:i:s' );
			$this->table->insert ( $data );
			
			$id = $this->table->getLastInsertValue ();
			$resultSet = $this->table->select ( array (
					'id' => $id 
			) );
			
			if (0 === count ( $resultSet )) {
				throw new \Exception ( 'Insert operation failed or did not result in new row', 500 );
			} else {
				$response ['response'] ['params'] = $data;
				$response ['response'] ['message'] = 'Success insert data transactions';
				$response ['response'] ['code'] = '';
				return $response;
			}
		} catch ( \Exception $e ) {
			$response ['response'] ['params'] = $data;
			$response ['response'] ['message'] = $e->getMessage () . ' and Failed insert data';
			$response ['response'] ['code'] = $e->getCode ();
			return $response;
		}
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return Entity
	 */
	public function fetch($id) {
		try {
			
			if (! $id) {
				throw new \Exception ( 'wrong id ', 500 );
			}
			$sqlSelect = $this->table->getSql ()->select ();
			$sqlSelect->columns ( array (
					'*' 
			) );
			$where = new Where ();
			$where->expression ( "transactions.invoice = ?", $id );
			$sqlSelect->where ( $where );
			
			$statement = $this->table->getSql ()->prepareStatementForSqlObject ( $sqlSelect );
			$rowSet = $statement->execute ();
			if (0 === count ( $rowSet )) {
				throw new \Exception ( 'no data found for invoice :' . $id, 500 );
			}
			$response ['response'] ['params'] = $rowSet->current ();
			$response ['response'] ['message'] = '';
			$response ['response'] ['code'] = '';
			return $response;
		} catch ( \Exception $e ) {
			$response ['response'] ['params'] = '';
			$response ['response'] ['message'] = $e->getMessage ();
			$response ['response'] ['code'] = $e->getCode ();
			return $response;
		}
	}
	
	/**
	 *
	 * @return Collection
	 */
	public function fetchAll() {
		$resultSet = $this->table->select ( function (Select $select) {
			$select->columns ( array (
					'*' 
			) );
		} );
		
		return $resultSet;
	}
	
	 
	public function update($invoice, $data) {
		try {
			
			if (empty ( $invoice )) {
				throw new DomainException ( 'invoice no found', 404 );
			}
			
			if (is_object ( $data )) {
				$data = ( array ) $data;
			}
			
			$dataUpdate ['status'] = $data ['status'];
			$data ['updated_at'] = date ( 'Y-m-d H:i:s' );
			
			$this->table->update ( $dataUpdate, array (
					'invoice' => $invoice 
			) );
			
			$rowSetTransactionsAddress = $this->TransactionsTable->select ( array (
					'invoice' => $invoice 
			) );
			
			$Transactions = $rowSetTransactionsAddress->current ();
			
			$rowSet = $this->table->select ( array (
					'invoice' => $invoice 
			) );
			
			if (0 === count ( $rowSet )) {
				throw new \Exception('Invoice not found','500');
			}
			
			$response ['response'] ['params'] = $data;
			$response ['response'] ['message'] = 'success update data transactions';
			$response ['response'] ['code'] = '';
			
			return $response;
		} catch ( \Exception $e ) {
			$response ['response'] ['params'] = $data;
			$response ['response'] ['message'] = 'failed update data transactions';
			$response ['response'] ['code'] = $e->getCode();
			return $response;
		}
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return bool
	 */
	public function delete($id) {
		if (! $id) {
			throw new DomainException ( 'Invalid identifier provided', 404 );
		}
		return $this->table->delete ( array (
				'id' => $id 
		) );
	}
	
	/*
	 * jika user belum ada di database add saldo
	 * jika sudah ada tambahkan nilai saldonya
	 */
	function checkUserIsExist($username) {
		$sqlSelect = $this->UsersTable->getSql ()->select ();
		$sqlSelect->columns ( array (
				'*' 
		) );
		$where = new Where ();
		$where->expression ( "username = ?", $username );
		$sqlSelect->where ( $where );
		$statement = $this->UsersTable->getSql ()->prepareStatementForSqlObject ( $sqlSelect );
		$rowSet = $statement->execute ();
		$result = $rowSet->current ();
		if (empty ( $result )) {
			return true;
		} else {
			return false;
		}
	}
	function checkReferenceNumber($number, $userId) {
		try {
			$sqlSelect = $this->ReferencesTable->getSql ()->select ();
			$sqlSelect->columns ( array (
					'*' 
			) );
			$where = new Where ();
			$where->expression ( "reference_number = ?", $number );
			$where->expression ( "user_id = ?", $userId );
			
			$sqlSelect->where ( $where );
			$statement = $this->ReferencesTable->getSql ()->prepareStatementForSqlObject ( $sqlSelect );
    	$rowSet = $statement->execute ();
    	$result = $rowSet->current ();
    	if(empty($result)) {
    		return false;
    	}else {
    		return true;
    	}
    }catch (\Exception $e) {
    	return false;
    }
    
    }
    
    
}
