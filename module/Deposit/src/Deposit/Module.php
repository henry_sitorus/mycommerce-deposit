<?php

namespace Deposit;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface {
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}
	public function getAutoloaderConfig() {
		return array (
				'ZF\Apigility\Autoloader' => array (
						'namespaces' => array (
								__NAMESPACE__ => __DIR__ 
						) 
				) 
		);
	}
	public function getServiceConfig() {
		return array (
				'factories' => array (
						'Users\Db\Adapter' => function ($sm) {
							$config = $sm->get ( 'config' );
							$adapter = new \Zend\Db\Adapter\Adapter ( $config ['db'] ['adapters'] ['db'] );
							return $adapter;
						},
						'Transactions\Db\Adapter' => function ($sm) {
							$config = $sm->get ( 'config' );
							$adapter = new \Zend\Db\Adapter\Adapter ( $config ['db'] ['adapters'] ['db'] );
							return $adapter;
						},
						'References\Db\Adapter' => function ($sm) {
							$config = $sm->get ( 'config' );
							$adapter = new \Zend\Db\Adapter\Adapter ( $config ['db'] ['adapters'] ['db'] );
							return $adapter;
						},
						'Balances\Db\Adapter' => function ($sm) {
						$config = $sm->get ( 'config' );
						$adapter = new \Zend\Db\Adapter\Adapter ( $config ['db'] ['adapters'] ['db'] );
						return $adapter;
						}
				) 
		)
		;
	}
}
