<?php
return array (
		'users' => array (
				'db' => 'Users\\Db\\Adapter',
				'table' => 'users' 
		),
		'transactions' => array (
				'db' => 'Transactions\\Db\\Adapter',
				'table' => 'transactions' 
		),
		'references' => array (
				'db' => 'References\\Db\\Adapter',
				'table' => 'references' 
		),
		'balances' => array (
				'db' => 'Balances\\Db\\Adapter',
				'table' => 'users' 
		),
		
		'service_manager' => array (
				'factories' => array (
						'Deposit\\V1\\Rest\\Users\\UsersResource' => 'Deposit\\V1\\Rest\\Users\\Factory\\UsersResourceFactory',
						'Users\\TableGatewayMapper' => 'Deposit\\V1\\Rest\\Users\\Factory\\UsersTableGatewayMapperFactory',
						'Users\\TableGateway' => 'Deposit\\V1\\Rest\\Users\\Factory\\UsersTableGatewayFactory',
						
						'Deposit\\V1\\Rest\\Transactions\\TransactionsResource' => 'Deposit\\V1\\Rest\\Transactions\\Factory\\TransactionsResourceFactory',
						'Transactions\\TableGatewayMapper' => 'Deposit\\V1\\Rest\\Transactions\\Factory\\TransactionsTableGatewayMapperFactory',
						'Transactions\\TableGateway' => 'Deposit\\V1\\Rest\\Transactions\\Factory\\TransactionsTableGatewayFactory',
						
						'Transactions\\UsersTableGateway' => 'Deposit\\V1\\Rest\\Transactions\\Factory\\UsersTableGatewayFactory', // ini untuk di lewatkan ke module users untuk load sb user
						'Transactions\\ReferencesTableGateway' => 'Deposit\\V1\\Rest\\Transactions\\Factory\\ReferencesTableGatewayFactory', // ini untuk di lewatkan ke module users untuk load sb user
						
						'Deposit\\V1\\Rest\\References\\ReferencesResource' => 'Deposit\\V1\\Rest\\References\\Factory\\ReferencesResourceFactory',
						'References\\TableGatewayMapper' => 'Deposit\\V1\\Rest\\References\\Factory\\ReferencesTableGatewayMapperFactory',
						'References\\TableGateway' => 'Deposit\\V1\\Rest\\References\\Factory\\ReferencesTableGatewayFactory',
						
						'Deposit\\V1\\Rest\\Balances\\BalancesResource' => 'Deposit\\V1\\Rest\\Balances\\Factory\\BalancesResourceFactory',
						'Balances\\TableGatewayMapper' => 'Deposit\\V1\\Rest\\Balances\\Factory\\BalancesTableGatewayMapperFactory',
						'Balances\\TableGateway' => 'Deposit\\V1\\Rest\\Balances\\Factory\\BalancesTableGatewayFactory' 
						
				
						
				)
				 
		),
		'router' => array (
				'routes' => array (
						'deposit.rest.users' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '/users[/:users_id]',
										'defaults' => array (
												'controller' => 'Deposit\\V1\\Rest\\Users\\Controller' 
										) 
								) 
						),
						'deposit.rest.transactions' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '/transactions[/:transactions_id]',
										'defaults' => array (
												'controller' => 'Deposit\\V1\\Rest\\Transactions\\Controller' 
										) 
								) 
						),
						'deposit.rest.references' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '/references[/:references_id]',
										'defaults' => array (
												'controller' => 'Deposit\\V1\\Rest\\References\\Controller' 
										) 
								) 
						),
						'deposit.rest.balances' => array (
								'type' => 'Segment',
								'options' => array (
										'route' => '/balances[/:balances_id]',
										'defaults' => array (
												'controller' => 'Deposit\\V1\\Rest\\Balances\\Controller' 
										) 
								) 
						) 
				) 
		),
		'zf-versioning' => array (
				'uri' => array (
						0 => 'deposit.rest.users',
						1 => 'deposit.rest.transactions',
						2 => 'deposit.rest.references',
						3 => 'deposit.rest.balances' 
				) 
		),
		'zf-rest' => array (
				'Deposit\\V1\\Rest\\Users\\Controller' => array (
						'listener' => 'Deposit\\V1\\Rest\\Users\\UsersResource',
						'route_name' => 'deposit.rest.users',
						'route_identifier_name' => 'users_id',
						'collection_name' => 'users',
						'entity_http_methods' => array (
								0 => 'GET',
								1 => 'PATCH',
								2 => 'PUT',
								3 => 'DELETE' 
						),
						'collection_http_methods' => array (
								0 => 'GET',
								1 => 'POST' 
						),
						'collection_query_whitelist' => array (),
						'page_size' => 25,
						'page_size_param' => null,
						'entity_class' => 'Deposit\\V1\\Rest\\Users\\UsersEntity',
						'collection_class' => 'Deposit\\V1\\Rest\\Users\\UsersCollection',
						'service_name' => 'Users' 
				),
				'Deposit\\V1\\Rest\\Transactions\\Controller' => array (
						'listener' => 'Deposit\\V1\\Rest\\Transactions\\TransactionsResource',
						'route_name' => 'deposit.rest.transactions',
						'route_identifier_name' => 'transactions_id',
						'collection_name' => 'transactions',
						'entity_http_methods' => array (
								0 => 'GET',
								1 => 'PATCH',
								2 => 'PUT',
								3 => 'DELETE' 
						),
						'collection_http_methods' => array (
								0 => 'GET',
								1 => 'POST' 
						),
						'collection_query_whitelist' => array (),
						'page_size' => 25,
						'page_size_param' => null,
						'entity_class' => 'Deposit\\V1\\Rest\\Transactions\\TransactionsEntity',
						'collection_class' => 'Deposit\\V1\\Rest\\Transactions\\TransactionsCollection',
						'service_name' => 'Transactions' 
				),
				'Deposit\\V1\\Rest\\References\\Controller' => array (
						'listener' => 'Deposit\\V1\\Rest\\References\\ReferencesResource',
						'route_name' => 'deposit.rest.references',
						'route_identifier_name' => 'references_id',
						'collection_name' => 'references',
						'entity_http_methods' => array (
								0 => 'GET',
								1 => 'PATCH',
								2 => 'PUT',
								3 => 'DELETE' 
						),
						'collection_http_methods' => array (
								0 => 'GET',
								1 => 'POST' 
						),
						'collection_query_whitelist' => array (),
						'page_size' => 25,
						'page_size_param' => null,
						'entity_class' => 'Deposit\\V1\\Rest\\References\\ReferencesEntity',
						'collection_class' => 'Deposit\\V1\\Rest\\References\\ReferencesCollection',
						'service_name' => 'References' 
				),
				'Deposit\\V1\\Rest\\Balances\\Controller' => array (
						'listener' => 'Deposit\\V1\\Rest\\Balances\\BalancesResource',
						'route_name' => 'deposit.rest.balances',
						'route_identifier_name' => 'balances_id',
						'collection_name' => 'balances',
						'entity_http_methods' => array (
								0 => 'GET',
								1 => 'PATCH',
								2 => 'PUT',
								3 => 'DELETE' 
						),
						'collection_http_methods' => array (
								0 => 'GET',
								1 => 'POST' 
						),
						'collection_query_whitelist' => array (),
						'page_size' => 25,
						'page_size_param' => null,
						'entity_class' => 'Deposit\\V1\\Rest\\Balances\\BalancesEntity',
						'collection_class' => 'Deposit\\V1\\Rest\\Balances\\BalancesCollection',
						'service_name' => 'Balances' 
				) 
		),
		'zf-content-negotiation' => array (
				'controllers' => array (
						'Deposit\\V1\\Rest\\Users\\Controller' => 'HalJson',
						'Deposit\\V1\\Rest\\Transactions\\Controller' => 'HalJson',
						'Deposit\\V1\\Rest\\References\\Controller' => 'HalJson',
						'Deposit\\V1\\Rest\\Balances\\Controller' => 'HalJson' 
				),
				'accept_whitelist' => array (
						'Deposit\\V1\\Rest\\Users\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/hal+json',
								2 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\Transactions\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/hal+json',
								2 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\References\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/hal+json',
								2 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\Balances\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/hal+json',
								2 => 'application/json' 
						) 
				),
				'content_type_whitelist' => array (
						'Deposit\\V1\\Rest\\Users\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\Transactions\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\References\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/json' 
						),
						'Deposit\\V1\\Rest\\Balances\\Controller' => array (
								0 => 'application/vnd.deposit.v1+json',
								1 => 'application/json' 
						) 
				) 
		),
		'zf-hal' => array (
				'metadata_map' => array (
						'Deposit\\V1\\Rest\\Users\\UsersEntity' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.users',
								'route_identifier_name' => 'users_id',
								'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable' 
						),
						'Deposit\\V1\\Rest\\Users\\UsersCollection' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.users',
								'route_identifier_name' => 'users_id',
								'is_collection' => true 
						),
						'Deposit\\V1\\Rest\\Transactions\\TransactionsEntity' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.transactions',
								'route_identifier_name' => 'transactions_id',
								'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable' 
						),
						'Deposit\\V1\\Rest\\Transactions\\TransactionsCollection' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.transactions',
								'route_identifier_name' => 'transactions_id',
								'is_collection' => true 
						),
						'Deposit\\V1\\Rest\\References\\ReferencesEntity' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.references',
								'route_identifier_name' => 'references_id',
								'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable' 
						),
						'Deposit\\V1\\Rest\\References\\ReferencesCollection' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.references',
								'route_identifier_name' => 'references_id',
								'is_collection' => true 
						),
						'Deposit\\V1\\Rest\\Balances\\BalancesEntity' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.balances',
								'route_identifier_name' => 'balances_id',
								'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable' 
						),
						'Deposit\\V1\\Rest\\Balances\\BalancesCollection' => array (
								'entity_identifier_name' => 'id',
								'route_name' => 'deposit.rest.balances',
								'route_identifier_name' => 'balances_id',
								'is_collection' => true 
						) 
				) 
		) 
);
